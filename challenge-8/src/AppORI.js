import logo from "./logo.svg";
import "./App.css";
import PlayerCreate from "./components/Pages/PlayerCreate";

function App() {
  return (
    <div>
      <PlayerCreate />
    </div>
  );
}

export default App;
