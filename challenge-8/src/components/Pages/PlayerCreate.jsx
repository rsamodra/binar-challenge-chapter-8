import React, { Component } from "react";
import Text from "../Text/Text";
import Password from "../Password/Password";
import Email from "../Email/Email";

class PlayerCreate extends Component {
  state = {
    username: "",
    email: "",
    password: "",
    experience: "",
    level: "",
    showResult: false,
  };

  changeHandler = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
      showResult: false,
    });
  };

  showHandler = () => {
    this.setState({
      showResult: true,
    });
  };

  render() {
    return (
      <div>
        <h1>Create Player</h1>

        <div className="container col-sm-3">
          <Text
            name="username"
            onChange={this.changeHandler}
            placeholder="UserName"
          />
          <Password
            name="password"
            onChange={this.changeHandler}
            placeholder="Password"
          />
          <Email
            name="email"
            onChange={this.changeHandler}
            placeholder="Email"
          />
          <Text
            name="experience"
            onChange={this.changeHandler}
            placeholder="Experience"
          />
          <Text
            name="level"
            onChange={this.changeHandler}
            placeholder="Input Level"
          />

          <button className="btn btn-primary m-3" onClick={this.showHandler}>
            Create Player
          </button>
          <br />
          {this.state.showResult && (
            <ul className="list-group list-group-flush">
              <li className="list-group-item">
                Username: {this.state.username}
              </li>
              <li className="list-group-item">Email: {this.state.email}</li>
              <li className="list-group-item">
                Password: {this.state.password}
              </li>
              <li className="list-group-item">
                Experience: {this.state.experience}
              </li>
              <li className="list-group-item">Level: {this.state.level}</li>
            </ul>
          )}
        </div>
      </div>
    );
  }
}

export default PlayerCreate;
