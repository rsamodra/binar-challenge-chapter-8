import React, { Component } from "react";
import Text from "../Text/Text";
import Password from "../Password/Password";
import Email from "../Email/Email";

class PlayerUpdate extends Component {
  state = {
    username: "Ayam",
    email: "Ayam@gmail.com",
    password: "Ayam",
    experience: 10,
    level: 10,
    showResult: false,
  };

  changeHandler = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
      showResult: false,
    });
  };

  showHandler = () => {
    this.setState({
      showResult: true,
    });
  };

  render() {
    return (
      <div>
        <h1>Update Player</h1>
        <div className="container col-sm-3">
          <Text
            name="username"
            value={this.state.username}
            onChange={this.changeHandler}
            placeholder="Input UserName"
          />
          <Password
            name="password"
            value={this.state.password}
            onChange={this.changeHandler}
            placeholder="Input Password"
          />
          <Email
            name="email"
            value={this.state.email}
            onChange={this.changeHandler}
            placeholder="Input Email"
          />
          <Text
            name="experience"
            value={this.state.experience}
            onChange={this.changeHandler}
            placeholder="Input Experience"
          />
          <Text
            name="Level"
            onChange={this.changeHandler}
            placeholder="Input Level"
          />

          <button className="btn btn-danger m-3" onClick={this.showHandler}>
            Update Player
          </button>
          <br />
          {this.state.showResult && (
            <ul className="list-group list-group-flush">
              <li className="list-group-item">
                Username: {this.state.username}
              </li>
              <li className="list-group-item">Email: {this.state.email}</li>
              <li className="list-group-item">
                Password: {this.state.password}
              </li>
              <li className="list-group-item">
                Experience: {this.state.experience}
              </li>
              <li className="list-group-item">Level: {this.state.level}</li>
            </ul>
          )}
        </div>
      </div>
    );
  }
}

export default PlayerUpdate;
