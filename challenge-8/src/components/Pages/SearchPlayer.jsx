import React, { Component } from "react";
import Text from "../Text/Text";
import Email from "../Email/Email";

class SearchPlayer extends Component {
  state = {
    username: "",
    email: "",
    password: "",
    experience: "",
    level: "",
    message: "",
    found: false,
    showResult: false,
    players: [
      {
        username: "sapi",
        email: "sapi@gmail.com",
        password: "sapi",
        experience: "100",
        level: "20",
      },
      {
        username: "kambing",
        email: "kambing@gmail.com",
        password: "kambing",
        experience: "90",
        level: "30",
      },
      {
        username: "ayam",
        email: "ayam@gmail.com",
        password: "ayam",
        experience: "70",
        level: "100",
      },
    ],
  };

  findPlayer = () => {
    var data;

    if (this.state.username) {
      data = this.state.players.find(
        ({ username }) => username === this.state.username.toLowerCase()
      );
    } else if (this.state.email) {
      data = this.state.players.find(
        ({ email }) => email === this.state.email.toLowerCase()
      );
    } else if (this.state.experience) {
      data = this.state.players.find(
        ({ experience }) => experience === this.state.experience
      );
    } else if (this.state.level) {
      data = this.state.players.find(({ level }) => level === this.state.level);
    }

    console.log(data, this.state);

    data
      ? this.setState({
          username: data.username,
          email: data.email,
          password: data.password,
          experience: data.experience,
          level: data.level,
          message: "Success, Player Found",
          found: true,
          showResult: true,
        })
      : this.setState({
          message: "Player Not Found or Search Can't be blank...",
          found: false,
          showResult: true,
        });
  };

  showHandler = () => {
    this.setState({
      showResult: true,
    });
  };

  changeHandler = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
      showResult: false,
    });
  };

  render() {
    return (
      <div>
        <h1>Search Player</h1>
        <div className="container col-sm-3">
          <Text
            name="username"
            onChange={this.changeHandler}
            placeholder="Search by UserName"
          />
          <Email
            name="email"
            onChange={this.changeHandler}
            placeholder="Search by Email"
          />
          <Text
            name="experience"
            onChange={this.changeHandler}
            placeholder="Search by Experience"
          />
          <Text
            name="Level"
            onChange={this.changeHandler}
            placeholder="Search by Level"
          />

          <button className="btn btn-warning m-3" onClick={this.findPlayer}>
            Search Player
          </button>
        </div>

        <div className="container col-sm-4">
          {this.state.showResult && !this.state.warning && (
            <span className="list-group-item bg-warning">
              {this.state.message}
            </span>
          )}
          {this.state.showResult && this.state.found && (
            <ul className="list-group list-group-flush">
              {/* <li className="list-group-item">{this.state.message}</li> */}
              <li className="list-group-item">
                Username: {this.state.username}
              </li>
              <li className="list-group-item">Email: {this.state.email}</li>
              <li className="list-group-item">
                Password: {this.state.password}
              </li>
              <li className="list-group-item">
                Experience: {this.state.experience}
              </li>
              <li className="list-group-item">Level: {this.state.level}</li>
            </ul>
          )}
        </div>
      </div>
    );
  }
}

export default SearchPlayer;
