import React, { Component } from "react";

class Email extends Component {
  render() {
    const { value, placeholder, name, onChange } = this.props;
    return (
      <input
        type="email"
        value={value}
        onChange={onChange}
        placeholder={placeholder}
        name={name}
        className="form-control mb-1"
      />
    );
  }
}

export default Email;
