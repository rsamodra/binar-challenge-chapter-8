import React, { Component } from "react";

class Text extends Component {
  render() {
    const { value, placeholder, name, onChange } = this.props;
    return (
      <input
        type="text"
        value={value}
        onChange={onChange}
        placeholder={placeholder}
        name={name}
        className="form-control mb-1"
      />
    );
  }
}

export default Text;
