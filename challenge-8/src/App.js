import "./App.css";
import { Component } from "react";
import PlayerCreate from "./components/Pages/PlayerCreate";
import PlayerUpdate from "./components/Pages/PlayerUpdate";
import SearchPlayer from "./components/Pages/SearchPlayer";

class App extends Component {
  state = {
    activeMenu: "",
  };

  menuHandler = (e) => {
    this.setState({ activeMenu: e.target.id });
  };

  render() {
    return (
      <div className="App">
        <nav className="navbar  navbar-expand-lg  navbar-light bg-light">
          <div className="container-fluid">
            <a className="navbar-brand nav-link">#PlayerOption</a>
            <div className="collapse navbar-collapse" id="navbarNav">
              <a
                className="nav-link"
                id="PlayerCreate"
                onClick={this.menuHandler}
              >
                Create
              </a>
              <a
                className="nav-link"
                id="PlayerUpdate"
                onClick={this.menuHandler}
              >
                Update
              </a>

              <a
                className="nav-link"
                id="SearchPlayer"
                onClick={this.menuHandler}
              >
                Search
              </a>
            </div>
          </div>
        </nav>

        {this.state.activeMenu === "PlayerCreate" && <PlayerCreate />}
        {this.state.activeMenu === "PlayerUpdate" && <PlayerUpdate />}
        {this.state.activeMenu === "SearchPlayer" && <SearchPlayer />}
      </div>
    );
  }
}

export default App;
